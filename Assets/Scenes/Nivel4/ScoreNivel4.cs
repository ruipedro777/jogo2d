using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreNivel4 : MonoBehaviour
{
    public int scoreValue = 0;
    Text score;

    private void Start()
    {
        score = GetComponent<Text>();

    }

    public void ResetScpore()
    {
        scoreValue = 0;
    }

    private void Update()
    {
        score.text = "SCORE: " + scoreValue;
    }
}
