using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManagerNivel4 : MonoBehaviour
{
    ScoreNivel4 score;
    void Start()
    {
        score = FindObjectOfType<ScoreNivel4>();
    }

    // Update is called once per frame
    void Update()
    {
        if (score.scoreValue >= 24)
        {
            SceneManager.LoadScene(0);
        }
    }
}
