using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    ScoreScript score;
    void Start()
    {
        score = FindObjectOfType<ScoreScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(score.scoreValue >= 24)
        {
            SceneManager.LoadScene(0);
        }
    }
}
