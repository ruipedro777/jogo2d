using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemy : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject enemy;
    private float spawnWait = 0f;
    private float spawnMostWait = 1.5f;
    private float spawnLeastWait = 1.0f;
    private float startWait = 2.0f;

    private void Start()
    {
        StartCoroutine(waveEnemies());
    }

    private void Update()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    IEnumerator waveEnemies()
    {

        yield return new WaitForSeconds(startWait);

        while (enemy != null)
        {
            int randSpawnPoints = Random.Range(0, spawnPoints.Length);
            Instantiate(enemy, spawnPoints[randSpawnPoints].position, Quaternion.identity);

            yield return new WaitForSeconds(spawnWait);
        }


    }
}
