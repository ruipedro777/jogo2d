using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void HowToPlay()
    {
        SceneManager.LoadScene(1);
    }

   //##################################################################

    //OPO42

    public void OPO42()
    {
        SceneManager.LoadScene(2);
    }

    public void BackToPlayOPO42()
    {
        SceneManager.LoadScene(3);
    }

    //################################

    //ParagemOnline
    public void ParagemOnline()
    {
        SceneManager.LoadScene(4);
    }
    public void GameOverParagemOnline()
    {
        SceneManager.LoadScene(5);
    }

    //################################

    public void ClassicTimer()
    {
        SceneManager.LoadScene(4);
    }

    public void BackToPlayClassicTimer()
    {
        SceneManager.LoadScene(5);
    }

    //#################################

    //KillGuys

    public void KillGuys()
    {
        SceneManager.LoadScene(6);
    }

    public void GameOverKillGuys()
    {
        SceneManager.LoadScene(7);
    }


    //###################################

    //AirTimer

    public void AirTimer()
    {
        SceneManager.LoadScene(8);
    }

    public void GameOverAirTimer()
    {
        SceneManager.LoadScene(9);
    }
    //###################################

    //Sow

    public void SOW()
    {
        SceneManager.LoadScene(10);
    }

    public void GameOverSow()
    {
        SceneManager.LoadScene(11);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    

    
 

    

    
}
