using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class tabelaMove : MonoBehaviour
{

    [SerializeField] Transform[] waypoints;
    [SerializeField] float moveSpeed = 1;
    int waypointIndex = 0;

    float currentTime;

    private void Update()
    {
        currentTime -= Time.deltaTime;


        if (currentTime <= 2)
        {
            if (Vector3.Distance(waypoints[waypointIndex].transform.position, transform.position) < 0.05f)
            {
                waypointIndex++;

                if (waypointIndex >= waypoints.Length)
                {
                    waypointIndex = 0;

                }
                currentTime = 3;

            }

            transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].transform.position,
                                                  moveSpeed * Time.deltaTime);
        }





    }
}
