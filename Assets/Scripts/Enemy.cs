using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        int RandSpeed = Random.Range(3, 5);

        rb.velocity = new Vector2(-RandSpeed, 0);


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("paredeMorte"))
        {
            Destroy(this.gameObject);
        }

    }
}
