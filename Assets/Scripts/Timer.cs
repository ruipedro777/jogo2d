using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{
    Text textTimer;
    private float currentTime = 0f;
    private float startTime = 35f;
    ScoreScript score;


    void Start()
    {
        currentTime = startTime;
        textTimer = GetComponent<Text>();
        score = FindObjectOfType<ScoreScript>();
    }

    
    void Update()
    {

        currentTime -= 1 * Time.deltaTime;
        textTimer.text = currentTime.ToString("F2");

        if(currentTime <= 15 && currentTime > 10)
        {
            textTimer.color = Color.yellow;

            
        }
        else if (currentTime <= 10)
        {
            
            
                textTimer.color = Color.red;

                if (currentTime <= 0)
                {
                    currentTime = 0;
                    SceneManager.LoadScene(5);
                    score.ResetScpore();
                }
            
        }
        
        
    }
}
