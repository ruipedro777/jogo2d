using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    public float speed;
    public float forceJump2, forceJump3, forceJump4, forceJump5;

    private Rigidbody2D myBody;

    ScoreScript score;
    ScoreNivel4 scoreNivel4;
    private bool allowMove;
    SpriteRenderer sp;

    private bool combo, combo2, comboNivel4, comboNivel401, comboNivel402, comboNivel403;
    //private bool apenasOnePoint;

    // Use this for initialization
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();

        score = FindObjectOfType<ScoreScript>();
        scoreNivel4 = FindObjectOfType<ScoreNivel4>();

        allowMove = true;

        sp = GetComponent<SpriteRenderer>();
        combo = false;
        combo2 = false;
        comboNivel4 = false;
    }


    void FixedUpdate()
    {
        if (allowMove == true)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

                if (touchPos.x > 0)
                {
                    myBody.velocity = new Vector2(speed, myBody.velocity.y);
                    transform.Rotate(0, 0, -5, Space.Self);
                }
                if (touchPos.x < 0)
                {
                    myBody.velocity = new Vector2(-speed, myBody.velocity.y);
                    transform.Rotate(0, 0, 5, Space.Self);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("tabela")) // score
        {
            score.scoreValue += 2;
            
            

        }

        if (collision.gameObject.CompareTag("tabela") && combo == true && combo2 == true)
        {
          
            score.scoreValue += 4;
            combo = false;
            combo2 = false;
            
        }
        //////////////////////////////////////////////////////////////////////////////////
        ///Nivel 4

        if (collision.gameObject.CompareTag("tabela4") && comboNivel401 == true && comboNivel4 == true)
        {
            scoreNivel4.scoreValue += 6;
            comboNivel4 = false;
            comboNivel401 = false;
        }

        if (collision.gameObject.CompareTag("tabela4") && comboNivel402 == true && comboNivel4 == true)
        {
            scoreNivel4.scoreValue += 4;
            comboNivel4 = false;
            comboNivel402 = false;
        }

        if (collision.gameObject.CompareTag("tabela4") && comboNivel403 == true && comboNivel4 == true)
        {
            scoreNivel4.scoreValue += 3;
            comboNivel4 = false;
            comboNivel403 = false;
        }

        if (collision.gameObject.CompareTag("tabela4")) // score
        {
            scoreNivel4.scoreValue += 2;
            comboNivel4 = false;

        }





        //if (collision.gameObject.CompareTag("tabela") && apenasOnePoint == true)
        //{
        //    score.scoreValue += 1;
        //    apenasOnePoint = false;
        //}


        if (collision.gameObject.CompareTag("mudaLayer")) // muda de layer quando entra no cesto
        {
            sp.sortingOrder = 1;
        }

        if (collision.gameObject.CompareTag("bolaPesada")) // nao permite mexer na bola quando entra no cesto
        {
            allowMove = false;
            
        }

        //nivel classico e nivel Timer
        if (collision.gameObject.CompareTag("trampoline25"))  //AMARELO
        {
            myBody.velocity = Vector2.up * forceJump2;
            combo = false;
        }

        if (collision.gameObject.CompareTag("trampoline50")) //AZUL
        {
            myBody.velocity = Vector2.up * forceJump3;
        }

        if (collision.gameObject.CompareTag("trampoline70")) //PRETO
        {
            myBody.velocity = Vector2.up * forceJump4;
        }

        if (collision.gameObject.CompareTag("trampoline80")) //VERMELO
        {
            myBody.velocity = Vector2.up * forceJump5;

            combo = false;
        }

        ////////////////////////////////////////////////////////////////////////
        ///
        // nivel 3

        if (collision.gameObject.CompareTag("vermelhoDireito"))
        {
            myBody.velocity = Vector2.left * 20;
            combo2 = true;
        }
        if (collision.gameObject.CompareTag("vermelhoEsquerda"))
        {
            myBody.velocity = -Vector2.up* 12;

            combo = true;
        }

        if (collision.gameObject.CompareTag("comboToFalse"))
        {
            combo = false;
            combo2 = true;
        }

        //##################################################################
        if (collision.gameObject.CompareTag("trampAmarelo25"))  //AMARELO
        {
            myBody.velocity = Vector2.one * forceJump2;
        }

        //##################################################################

       

        if (collision.gameObject.CompareTag("tubeAmareloNivel4"))  //AMARELO
        {
            comboNivel401 = true;
            myBody.velocity = Vector2.one * forceJump2;
           

        }

        if (collision.gameObject.CompareTag("tuboPretoNivel4"))  //PRETO
        {
            comboNivel402 = true;
            myBody.velocity = Vector2.one * forceJump2;
           

        }

        if (collision.gameObject.CompareTag("tubeVermelhoNivel4"))  //Vermelho
        {
            comboNivel403 = true;
            myBody.velocity = Vector2.one * forceJump2;
           

        }

        if (collision.gameObject.CompareTag("tubeAzulNivel4")) //AZUL
        {
            myBody.velocity = Vector2.up * forceJump3;
            comboNivel4 = false;
        }

        if (collision.gameObject.CompareTag("TubeReferenciaNivel4"))  //Vermelho
        {
            myBody.velocity = new Vector2(-1, 1) * 10;
            comboNivel4 = true;
        }


        //#####################################################
        // NIVEL 3
        if (collision.gameObject.CompareTag("tubeAmarelo2Nivel4"))  //AMARELO
        {
            myBody.velocity = Vector2.zero * forceJump2;
        }

        if (collision.gameObject.CompareTag("tuboPreto2Nivel3"))  //PRETO
        {
            myBody.velocity = Vector2.zero * forceJump2;
        }

        if (collision.gameObject.CompareTag("tubeVermelho2Nivel4"))  //Vermelho
        {
            myBody.velocity = Vector2.zero * forceJump2;
        }

        
        



        // GAMEOVERS

        if (collision.gameObject.CompareTag("GameOverEasy") || collision.gameObject.CompareTag("EnemyEasy"))
        {
            SceneManager.LoadScene(3);  // load da pagina gameOver
            score.ResetScpore();
        }

        if (collision.gameObject.CompareTag("GameOverTimer") || collision.gameObject.CompareTag("EnemyTimer"))
        {
            SceneManager.LoadScene(5);  // load da pagina gameOver
            score.ResetScpore();
        }

        if (collision.gameObject.CompareTag("GameOverNivel3") || collision.gameObject.CompareTag("EnemyNivel3"))
        {
            SceneManager.LoadScene(7); // load da pagina gameOver
            score.ResetScpore();
        }

        if (collision.gameObject.CompareTag("GameOverNivel4") || collision.gameObject.CompareTag("EnemyNivel4"))
        {
            SceneManager.LoadScene(9); // load da pagina gameOver
            score.ResetScpore();
        }

        //###############################################################

        if (collision.gameObject.CompareTag("GameHowToPlay")) // How to player layer
        {
            SceneManager.LoadScene(2);        
        }

      

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("bolaPesada"))
        {
            allowMove = true;
        }

        if (collision.gameObject.CompareTag("mudaLayer"))
        {
            sp.sortingOrder = 2;

        }

        
    }
}
