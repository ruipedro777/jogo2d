using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeMove : MonoBehaviour
{
    [SerializeField] Transform[] waypoints;
    [SerializeField] float moveSpeed = 1;
    int waypointIndex = 0;

    float currentTime;

    void Start()
    {
        //transform.position = waypoints[waypointIndex].transform.position;
        //StartCoroutine(MoveInCertainTime());

        //currentTime = Time.deltaTime;
    }

    //IEnumerator MoveTimer(Transform[] waypoints)
    //{
    //    while(Vector3.Distance(transform.position, waypoints[waypointIndex].transform.position) < 0.05f)
    //    {
    //        transform.position = Vector2.Lerp(transform.position, waypoints[waypointIndex].transform.position,
    //                                           moveSpeed * Time.deltaTime);

    //        yield return null;
    //    }

    //    yield return new WaitForSeconds(1f);
    //    //transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position,
    //    //                                        moveSpeed * Time.deltaTime);

    //    if (transform.position == waypoints[waypointIndex].transform.position)
    //    {
    //        waypointIndex += 1;
    //    }
    //    if (waypointIndex == waypoints.Length)
    //    {
    //        waypointIndex = 0;
    //    }
    //}

    private void Update()
    {
        currentTime -= Time.deltaTime;


        if (currentTime <= 2)
        {
            if (Vector3.Distance(waypoints[waypointIndex].transform.position, transform.position) < 0.05f)
            {
                waypointIndex++;

                if (waypointIndex >= waypoints.Length)
                {
                    waypointIndex = 0;

                }
                currentTime = 5;

            }

            transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].transform.position,
                                                  moveSpeed * Time.deltaTime);
        }





    }

    //IEnumerator MoveInCertainTime()
    //{
    //    yield return new WaitForSeconds(3);
    //    MoveTube();
    //}
    //private void MoveTube()
    //{

    //}
}
